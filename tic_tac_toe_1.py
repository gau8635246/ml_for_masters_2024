# Define the game board as a list of lists
board = [[" " for _ in range(3)] for _ in range(3)]

# Function to display the game board
def display_board():
  for row in board:
    print("|", end="")
    for cell in row:
      print(f" {cell} |", end="")
    print()

# Function to check if a player has won
def check_win(player):
  # Check rows
  for row in board:
    if all(cell == player for cell in row):
      return True
  # Check columns
  for col in range(3):
    if all(board[row][col] == player for row in range(3)):
      return True
  # Check diagonals
  if all(board[i][i] == player for i in range(3)) or all(board[i][2-i] == player for i in range(3)):
    return True
  return False

# Function to check if the board is full
def is_board_full():
  return all(cell != " " for row in board for cell in row)

# Function for player input
def player_move(player):
  while True:
    try:
      row, col = map(int, input(f"Player {player}, enter your move (row, col): ").split())
      if 0 <= row <= 2 and 0 <= col <= 2 and board[row][col] == " ":
        return row, col
      else:
        print("Invalid move. Try again.")
    except ValueError:
      print("Invalid input. Please enter two integers separated by a space.")

# Main game loop
game_on = True
current_player = "X"

while game_on:
  # Display the board
  display_board()

  # Get player move
  row, col = player_move(current_player)

  # Update the board
  board[row][col] = current_player

  # Check for winner
  if check_win(current_player):
    display_board()
    print(f"Player {current_player} wins!")
    game_on = False
  
  # Check for tie
  elif is_board_full():
    display_board()
    print("It's a tie!")
    game_on = False

  # Switch player
  current_player = "O" if current_player == "X" else "X"

print("Thanks for playing!")