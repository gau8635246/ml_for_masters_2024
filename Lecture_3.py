import matplotlib.pyplot as plot
import matplotlib
matplotlib.use('TkAgg')
from sklearn import linear_model

X = [ [27], [48], [53], [76], [89], [102] ]
Y = [27500, 41200, 67000, 95400, 90000, 97500]

Y = [27500, 11200, 6700, 9400, 9000, 9500]



regr = linear_model.LinearRegression()
regr.fit(X, Y)

print(regr.coef_)

print(regr.predict([[27], [100]]))



Y1 = regr.predict(X)
print(Y1)


plot.scatter(X, Y, color="green", marker="x")
plot.plot(X, Y1, color="red")
plot.xlabel("Area")
plot.ylabel("Price")
plot.show()




